classdef PSDB
    % Implementation of PSDB API calls 
    % PSDB API version 1
    %   For detailed explanation refer to online documentation.
    %   https://psdb.gsi.de/apidoc/
    %   Parameters are the same as in the document and needs to be paired.
    %   e.g. ('shot_id', 12345) or
    %   ('experiment_id', [54 67])
    
    properties (Constant = true, Hidden = true)
        % some default values to be set, but not shown.
        % but allow for override.
        PSDB_BaseUrl = 'https://psdb.gsi.de/api/';
        options_json = weboptions('ArrayFormat', 'php', 'Timeout', 45);
    end
    properties (SetAccess = private, GetAccess = public)
        logged_in = false;
    end
    properties (SetAccess = private, GetAccess = protected)
        single_access_token;
    end
    
    methods (Access = public)
        % constructor method
        function obj = PSDB()
            obj = obj.doLogin();
        end
        % check only logged in flag. And error msg.
        function TF = checkLoggedIn(obj)
            if obj.logged_in
                TF = true;
            else
                disp('Not (properly) logged in!');
                TF = false;
            end
        end
        
        % PSDB API methods
        function response = API_datatypes(obj)
            % return all available measurement data types such as numeric, string, etc.
            % No parameters
            % response = API_datatypes(obj)
            response = obj.API_BaseTypes('datatypes');
        end
        function response = API_shottypes(obj)
            % return all available shot types such as "experiment shot", "test shot", etc.
            % No parameters
            % response = API_shottypes(obj)
            response = obj.API_BaseTypes('shottypes');
        end
        function response = API_subsystems(obj)
            % return all available subsystems such as "MAS", "COS", etc.
            % No parameters
            % response = API_subsystems(obj)
            response = obj.API_BaseTypes('subsystems');
        end
        function response = API_classtypes(obj)
            % return all class types such as PH_gentec_Powermeter, etc.
            % No parameters
            % response = API_classtypes(obj)
            response = obj.API_BaseTypes('classtypes');
        end
        function response = API_experimentareas(obj)
            % return all available experiment areas such as PTA, Z6, etc.
            % No parameters
            % response = API_experimentareas(obj)
            response = obj.API_BaseTypes('experimentareas');
        end
        function response = API_experiments(obj)
            % This returns all experiments available to the current user.
            % No parameters
            % response = API_experiments(obj)
            response = [];
            if obj.checkLoggedIn
                webgrr = obj.requestParameterStringURL('experiments', struct([]));
                try
                    response = webread(webgrr, obj.options_json);
                    response = PSDB.parsePSDBTimestamp(response);
                catch ME
                    PSDB.checkMEError(ME);
                end
            end
        end
        function response = API_instancesSingle(obj, id)
            % returns a single system instance (device) with the given database id.
            % Parameters
            % a database instance id number
            % response = API_instancesSingle(obj,id)
            response = [];
            if obj.checkLoggedIn
                try
                    PSDB.checkNumScalar(id);
                    webgrr = obj.requestParameterStringURL(['instances/' num2str(id)], struct([]));
                    response = webread(webgrr, obj.options_json);
                    response = PSDB.parsePSDBTimestamp(response);
                catch ME
                    PSDB.checkMEError(ME);
                end
            end
        end
        function response = API_shotsSingle(obj, id)
            % return a single shot with the given id
            % Parameters
            % a database shot id number
            % response = API_shotsSingle(obj,id)
            response = [];
            if obj.checkLoggedIn
                try
                    PSDB.checkNumScalar(id);
                    webgrr = obj.requestParameterStringURL(['shots/' num2str(id)], struct([]));
                    response = webread(webgrr, obj.options_json);
                    response = PSDB.parsePSDBTimestamp(response);
                catch ME
                    PSDB.checkMEError(ME);
                end
            end
        end
        function response = API_instancesList(obj, varargin)
            % return system instances (devices) such as PA_Input_Nearfield_Cam, etc. If no parameters are given, all instances are returned. Otherwise the result is filtered by the given params.
            % Parameters
            % shot_id, subsystem_id, classtype_id
            % response = API_instancesList(obj,ParamName1,ParamValue1, ...)
            response = [];
            if obj.checkLoggedIn
                [parsedParameters, parseError] = PSDB.parsePSDBParameters(varargin);
                if ~parseError
                    webgrr = obj.requestParameterStringURL('instances', parsedParameters);
                    try
                        response = webread(webgrr, obj.options_json);
                        response = PSDB.parsePSDBTimestamp(response);
                    catch ME
                        PSDB.checkMEError(ME);
                    end
                end
            end
        end
        function response = API_shotsList(obj, varargin)
            % return all shots available to the user
            % Parameters
            % id, experiment_id, shottype_id, from_shot_id, to_shot_id, from_date, to_date
            % response = API_shotsList(obj,ParamName1,ParamValue1, ...)
            % Note: Dates have to be a datetime object, e.g.:
            %   datetime(2014,10,11); datetime(yyyy,mm,dd)
            response = [];
            if obj.checkLoggedIn
                [parsedParameters, parseError] = PSDB.parsePSDBParameters(varargin);
                if ~parseError
                    webgrr = obj.requestParameterStringURL('shots', parsedParameters);
                    try
                        response = webread(webgrr, obj.options_json);
                        response = PSDB.parsePSDBTimestamp(response);
                    catch ME
                        PSDB.checkMEError(ME);
                    end
                end
            end
        end
        function response = API_instancevalues(obj, varargin)
            % return a list of actual measurement data available to the user
            % Parameters
            % shot_id, instance_id, experiment_id, name, chunk
            % response = API_instancevalues(obj,ParamName1,ParamValue1, ...)
            response = [];
            if obj.checkLoggedIn
                [parsedParameters, parseError] = PSDB.parsePSDBParameters(varargin);
                if ~parseError
                    webgrr = obj.requestParameterStringURL('instancevalues', parsedParameters);
                    try
                        response = webread(webgrr, obj.options_json);
                        response = PSDB.parsePSDBTimestamp(response);
                    catch ME
                        PSDB.checkMEError(ME);
                    end
                end
            end
        end
        function response = API_instancevaluesChunks(obj, varargin)
            % return the number of data chunks (see command above)
            % Parameters
            % shot_id, instance_id, experiment_id, name
            % response = API_instancevaluesChunks(obj,ParamName1,ParamValue1, ...)
            response = [];
            if obj.checkLoggedIn
                [parsedParameters, parseError] = PSDB.parsePSDBParameters(varargin);
                if ~parseError
                    webgrr = obj.requestParameterStringURL('instancevalues/chunks', parsedParameters);
                    try
                        response = webread(webgrr, obj.options_json);
                        response = PSDB.parsePSDBTimestamp(response);
                    catch ME
                        PSDB.checkMEError(ME);
                    end
                end
            end
        end
        function response = API_attachments(obj, varargin)
            % This returns only file attachments available to the current user.
            % Parameters
            % shot_id, experiment_id, logentry_id, chunk
            % response = API_attachments(obj,ParamName1,ParamValue1, ...)
            response = [];
            if obj.checkLoggedIn
                [parsedParameters, parseError] = PSDB.parsePSDBParameters(varargin);
                if ~parseError
                    webgrr = obj.requestParameterStringURL('attachments', parsedParameters);
                    try
                        response = webread(webgrr, obj.options_json);
                        response = PSDB.parsePSDBTimestamp(response);
                    catch ME
                        PSDB.checkMEError(ME);
                    end
                end
            end
        end
        function response = API_attachmentsChunks(obj, varargin)
            % return the number of chunks for given filter parameters.
            % Parameters
            % shot_id, experiment_id, logentry_id, chunk
            % response = API_attachmentsChunks(obj,ParamName1,ParamValue1, ...)
            response = [];
            if obj.checkLoggedIn
                [parsedParameters, parseError] = PSDB.parsePSDBParameters(varargin);
                if ~parseError
                    webgrr = obj.requestParameterStringURL('attachments/chunks', parsedParameters);
                    try
                        response = webread(webgrr, obj.options_json);
                        response = PSDB.parsePSDBTimestamp(response);
                    catch ME
                        PSDB.checkMEError(ME);
                    end
                end
            end
        end
        function response = API_instancevaluesetsCount(obj, varargin)
            % Return the number of measurement value sets that would be downloaded with the given filter parameters. This function can be used for a "progress calculation" while downloading large amount of data.
            % Parameters
            % instance_id, shot_id
            % response = API_instancevaluesetsCount(obj,ParamName1,ParamValue1, ...)
            response = [];
            if obj.checkLoggedIn
                [parsedParameters, parseError] = PSDB.parsePSDBParameters(varargin);
                if ~parseError
                    webgrr = obj.requestParameterStringURL('instancevaluesets/count', parsedParameters);
                    try
                        response = webread(webgrr, obj.options_json);
                    catch ME
                        PSDB.checkMEError(ME);
                    end
                end
            end
        end
    end
    
    methods (Access = public, Static = true)
        function response = convertBase64(dataBi)
            base64 = org.apache.commons.codec.binary.Base64;
            y = base64.decode(uint8(dataBi));
            response = y(5:end);
        end
        function [x,y] = convertBase64Table(dataBi)
            base64 = org.apache.commons.codec.binary.Base64;
            y = char(base64.decode(uint8(dataBi)));
            C = textscan(y(5:end),'%f, %f');
            x = C{1};
            y = C{2};
        end
        function [strTable, unitHeaderLine] = convertBase64StringArray(dataBi, dataStr)
            base64 = org.apache.commons.codec.binary.Base64;
            y = char(base64.decode(uint8(dataBi)));
            y = y(5:end);

            aHeader = textscan(dataStr,'%[^\n\r]');
            cHeader = cellfun(@(x) textscan(x,'%q', 'Delimiter',','), aHeader{1});
            dHeader = {};
            for i=1:length(cHeader)
                % reshape and remove whitespace for headers.
                dHeader(i,:) = regexprep(cHeader{i}, '\W', '');
            end
            if size(dHeader,1) ~= 2
                dHeader = reshape(dHeader,[2,size(dHeader,2)/2])';
            end
            unitHeaderLine = dHeader(2,:);

            aBody = textscan(y,'%[^\n\r]');
            cBody = cellfun(@(x) textscan(x,'%q', 'Delimiter',','), aBody{1});
            dBody = {};
            for i=1:length(cBody)
                dBody(i,:) = cBody{i}';
            end
            strTable = cell2table(dBody,'VariableNames',dHeader(1,:));
        end
    end
    
    methods (Access = private)
        function obj = doLogin(obj)
            % login method, store only single access token
            obj.logged_in = false;
            options_json_login = weboptions('MediaType','application/json');
            prompt = {'Enter username:','Enter password:'};
            dlg_title = 'PSDB Login';
            num_lines = 1;
            defaultans = {'username','password'};
            answer = inputdlg(prompt,dlg_title,num_lines,defaultans);
            if ~isempty(answer) % user cancel
                autho = struct('user_session',struct('login',answer{1},'password',answer{2}));
                try
                    response = webwrite([obj.PSDB_BaseUrl 'user_sessions'],autho,options_json_login);
                    err = false;
                catch ME
                    PSDB.checkMEError(ME);
                    err = true;
                end
            else
                err = true;
                disp('Error: User abort!');
            end

            if err
                obj.single_access_token = [];
                obj.logged_in = false;
            else
                obj.single_access_token = response.single_access_token;
                obj.logged_in = true;
            end
        end
        function response = API_BaseTypes(obj, type)
            % API, no login, no parameters
            response = [];
            webgrr = matlab.internal.webservices.urlencode([obj.PSDB_BaseUrl type], obj.options_json);
            try
                response = webread(webgrr, obj.options_json);
                response = PSDB.parsePSDBTimestamp(response);
            catch ME
                err_msg = PSDB.checkMEError(ME);
                msgbox(err_msg,'Error','Error')
            end
        end
        function webgrr = requestParameterStringURL(obj, type, parsedParameters)
            % some silly workaround to parse parameters back to url
            % request ... no better idea!
            webgrr = matlab.internal.webservices.urlencode([obj.PSDB_BaseUrl type], obj.options_json, 'user_credentials', obj.single_access_token);
            testNames = fieldnames(parsedParameters);
            for ia = 1:length(testNames)
                abc = char(testNames(ia));
                tmpgrr = matlab.internal.webservices.urlencode('http://axz.com', obj.options_json, (abc), parsedParameters.(abc));
                tmpgrr = regexprep(tmpgrr,'http://axz.com\?','&');
                % unsave but no better idea.
                tmpgrr = strrep(tmpgrr, '+', ' ');
                webgrr = [webgrr tmpgrr];
            end
        end
    end
    
    methods (Access = private, Static = true)
        function checkMEError(ME)
            switch ME.identifier
                case 'MATLAB:webservices:HTTP401StatusCodeError'
                    disp('Error: No valid Login!');
                case 'MATLAB:webservices:HTTP404StatusCodeError'
                    disp('Error: URL not found or no access!');
                case 'MATLAB:InputParser:ParamMustBeChar'
                    disp(['Warning: ' ME.message]);
                case 'MATLAB:InputParser:ArgumentFailedValidation'
                    disp(['Warning: ' ME.message]);
                case 'MATLAB:InputParser:ParamMissingValue'
                    disp(['Warning: ' ME.message]);
                case 'PSDB:checkNum'
                    disp(['Warning: ' ME.message]);
                otherwise
                    rethrow(ME)
            end
        end
        function response = parsePSDBTimestamp(structIn)
            if ~isempty(structIn)
                field_names={'created_at'; 'updated_at'};
                % fields to replace. cell!
                for ii = 1:length(field_names)
                    if isfield(structIn, field_names{ii})
                        for i = 1:length(structIn)
                            % PSDB date string.
                            t = datetime(structIn(i).(field_names{ii}),'InputFormat','uuuu-MM-dd''T''HH:mm:ss.SSSXXX','TimeZone','UTC');
                            structIn(i).(field_names{ii}) = t;
                        end
                    end
                end
            end
            response = structIn;
        end
        function [parsedParameters, parseError] = parsePSDBParameters(parametersIn)
            parsedParameters = struct();
            parseError = true;
            p = inputParser;
            p.KeepUnmatched = true;
            p.PartialMatching = false;
            addParameter(p,'id',[],@PSDB.checkNum);
            addParameter(p,'shot_id',[],@PSDB.checkNum);
            addParameter(p,'subsystem_id',[],@PSDB.checkNum);
            addParameter(p,'classtype_id',[],@PSDB.checkNum);
            addParameter(p,'experiment_id',[],@PSDB.checkNum);
            addParameter(p,'logentry_id',[],@PSDB.checkNum);
            addParameter(p,'shottype_id',[],@PSDB.checkNum);
            addParameter(p,'from_date',[],@PSDB.checkDateScalar);
            addParameter(p,'to_date',[],@PSDB.checkDateScalar);
            addParameter(p,'from_shot_id',[],@PSDB.checkNumScalar);
            addParameter(p,'to_shot_id',[],@PSDB.checkNumScalar);
            addParameter(p,'instance_id',[],@PSDB.checkNum);
            addParameter(p,'chunk',[],@PSDB.checkNum);
            addParameter(p,'name',{},@PSDB.checkString);
            try
                parse(p,parametersIn{:});
                if ~isempty(fieldnames(p.Unmatched))
                    % if there is extra input, STOP!
                    disp('Extra/Unrecognized inputs:')
                    disp(p.Unmatched)
                    return
                end
    %             if ~isempty(p.UsingDefaults)
    %                 disp('Using defaults (discard):')
    %                 disp(p.UsingDefaults)
    %             end
                parsedParameters = p.Results;
                if ~isempty(parsedParameters)
                    testNames = fieldnames(parsedParameters);
                    for ia = 1:length(testNames)
                        abc = char(testNames(ia));
                        if isnumeric(parsedParameters.(abc))
                            parsedParameters.(abc) = sort(unique(parsedParameters.(abc)));
                        end
                    end
                end
                parsedParameters = rmfield(parsedParameters, (p.UsingDefaults));
                parseError = false;
            catch ME
                PSDB.checkMEError(ME);
            end
        end
        function TF = checkNum(x)
            TF = false;
            if ~isnumeric(x)
                % only numbers
                error('PSDB:checkNum','Input is not numeric.');
            elseif isempty(x)
                % need no empty input
                error('PSDB:checkNum','Input is empty.');
            elseif any(x <=0 )
                % everything greater zero
                error('PSDB:checkNum','Input has to > 0.');
            elseif any(~(x==round(x)))
                % check that there are only non fractional numbers
                error('PSDB:checkNum','Input expect only whole numbers.');
            else
                TF = true;
            end
        end
        function TF = checkNumScalar(x)
            TF = false;
            if ~PSDB.checkNum(x)
                % reuse code
            elseif ~isscalar(x)
                % only a scalar is accepted
                error('PSDB:checkNum','Input has to be a scalar.');
            else
                TF = true;
            end
        end
        function TF = checkDateScalar(x)
            TF = false;
            if ~isdatetime(x)
                % we need a datetime object
                error('PSDB:checkNum','Input has to be a datetime object (datetime(yyyy,mm,dd)).');
            elseif ~isscalar(x)
                % only a scalar is accepted
                error('PSDB:checkNum','Input has to be a scalar.');
            else
                TF = true;
            end
        end
        function TF = checkString(x)
            TF = true;
            if iscell(x)
                for k = 1:length(x)
                    if ~ischar(x{k})
                        % we need a String
                        error('PSDB:checkString','Input has to be string for given parameter.');
                        TF = false;
                    elseif ~isempty( regexp( x{k}, '[^\w\s?:]', 'start' ))
                        % we want to allow only alpha numeric in the string
                        error('PSDB:checkString','Input has to be alpha numeric string.');
                        TF = false;
                    end
                end
            else
                if ~ischar(x)
                    % we need a String
                    error('PSDB:checkString','Input has to be string for given parameter.');
                    TF = false;
                elseif ~isempty( regexp( x, '[^\w\s?:]', 'start' ))
                    % we want to allow only alpha numeric in the string
                    error('PSDB:checkString','Input has to be alpha numeric string (including whitespace and ?).');
                    TF = false;
                end
            end
        end
    end
    
end
