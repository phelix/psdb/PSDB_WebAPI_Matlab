% read PSDB Hamatsu images
% will create a folder '(expname)' within the current folder. There the
% images will be stored as png along with the shot number.
% filtered instancevalues by f_name_str. e.g. 'Image'
clearvars;
DataSource = PSDB;

% Define the experiment Name to ask for. You should have read permissions.
expName = 'I011'; % Brabetz/Wagner Wavefront improvement 2016 - Laserbay

% Hamamatsu instance id is 247
% can be found with the method 'API_instancesList' of PSDB.
instance = 247;

f_name_str = 'Image'; % name we look for

% get experiments list and find the experiment DB Id
experiments = DataSource.API_experiments;
expId = experiments(strcmpi(expName, {experiments.name})).id;

% get shot Id's that are 1 or 3 (experiment or snap shot)
% can be found with the method 'API_shottypes' of PSDB.
tmpShotList = DataSource.API_shotsList('experiment_id', expId, 'shottype_id', [1 3]);
shots = [tmpShotList.id];
% if you want to ask for specific shot numbers within the given experiment 
% you should use:
% shots = 13321:13323;

% don't change below
clear tmpShotList experiments;
DatasSize = DataSource.API_instancevaluesChunks('experiment_id', expId, 'shot_id', shots, 'instance_id', instance, 'name', f_name_str);
Datas=[];
for ii=1:DatasSize
    Datas = [Datas;
        DataSource.API_instancevalues('experiment_id', expId, 'shot_id', shots, 'instance_id', instance, 'name', f_name_str, 'chunk', ii)];
end
clear expId shots;

for i = 1:length(Datas)
    y = PSDB.convertBase64(Datas(i).data_binary);
    [~,~,~] = mkdir(expName);
    fid = fopen([expName '\i' num2str(instance) 'shotid' num2str(Datas(i).shot_id) '.png'], 'w');
    fwrite(fid, y, 'int8'); % dumb the raw binary to the hard disk
    clear y dataBi;
    fclose(fid);
end