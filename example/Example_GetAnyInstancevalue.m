% read any specific instance(s) from given shot(s) you have access to.
% stored in the variable 'Datas'
clearvars;
DataSource = PSDB;

% can be found with the method 'API_instancesList' of PSDB.
instance = [111 247];

% can be found with the method 'API_shotsList' of PSDB.
shots = [13321 13323];

% don't change below
DatasSize = DataSource.API_instancevaluesChunks('shot_id', shots, 'instance_id', instance);
Datas=[];
for ii=1:DatasSize
    Datas = [Datas;
        DataSource.API_instancevalues('shot_id', shots, 'instance_id', instance, 'chunk', ii)];
end
clear shots;