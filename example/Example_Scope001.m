% read PSDB Tektronix and write to disk a .csv
% will create a folder '(expname)' within the current folder.
% filtered instancevalues by f_name_str. e.g. 'Channel1: Data'
clearvars;
DataSource = PSDB;

% Define the experiment Name to ask for. You should have read permissions.
expName = 'P151';

% Tektronix 7000 001 instance id is 198
instance = 198;

f_name_str = 'Channel1: Data'; % name we look for

% get experiments list and find the experiment DB Id
experiments = DataSource.API_experiments;
expId = experiments(strcmpi(expName, {experiments.name})).id;

% get shot Id's that are 1 (experiment)
tmpShotList = DataSource.API_shotsList('experiment_id', expId, 'shottype_id', 1);

% don't change below
shots = [tmpShotList.id];
clear tmpShotList experiments;
DatasSize = DataSource.API_instancevaluesChunks('experiment_id', expId, 'shot_id', shots, 'instance_id', instance, 'name', f_name_str);
Datas=[];
for ii=1:DatasSize
    Datas = [Datas;
        DataSource.API_instancevalues('experiment_id', expId, 'shot_id', shots, 'instance_id', instance, 'name', f_name_str, 'chunk', ii)];
end
clear expId shots;

for i = 1:length(Datas)
    y = PSDB.convertBase64(Datas(i).data_binary);
    [~,~,~] = mkdir(expName);
    fid = fopen([expName '\i' num2str(instance) 'shotid' num2str(Datas(i).shot_id) '.csv'], 'w');
    fwrite(fid, y, 'int8'); % dumb the raw binary to the hard disk
    clear y dataBi;
    fclose(fid);
end