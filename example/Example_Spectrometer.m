% read PSDB Spectrometer and plot them ALL
% filtered instancevalues by f_name_str. e.g. 'Spectrum'
clearvars;
DataSource = PSDB;

% Define the experiment Name to ask for. You should have read permissions.
expName = 'I020/P190';

% MAS Spectrometer is 128
instance = 128;

f_name_str = 'Spectrum'; % name we look for

% get experiments list and find the experiment DB Id
experiments = DataSource.API_experiments;
expId = experiments(strcmpi(expName, {experiments.name})).id;

% get shot Id's that are 1 (experiment)
tmpShotList = DataSource.API_shotsList('experiment_id', expId, 'shottype_id', 1);

% don't change below
shots = [tmpShotList.id];
clear tmpShotList experiments;
DatasSize = DataSource.API_instancevaluesChunks('experiment_id', expId, 'shot_id', shots, 'instance_id', instance, 'name', f_name_str);
Datas=[];
for ii=1:DatasSize
    Datas = [Datas;
        DataSource.API_instancevalues('experiment_id', expId, 'shot_id', shots, 'instance_id', instance, 'name', f_name_str, 'chunk', ii)];
end
clear expId shots;

figure(4711)
hold on
for i = 1:length(Datas)
    [x,y] = PSDB.convertBase64Table(Datas(i).data_binary);
    plot(x,y,'DisplayName', ['shotid' num2str(Datas(i).shot_id)]);
end
hold off
grid on
grid minor
legend show
legend('Location', 'best');
xlabel('Wavelength in nm');
ylabel('Intensity in a.u.');