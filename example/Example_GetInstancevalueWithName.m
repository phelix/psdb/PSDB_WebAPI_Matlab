% read any specific instance(s) from given shot(s) you have access to.
% stored in the variable 'Datas'
% only the parameters you ask in 'name'
clearvars;
DataSource = PSDB;

% can be found with the method 'API_instancesList' of PSDB.
instance = 145;

% can be found with the method 'API_shotsList' of PSDB.
shots = [19345 19348];

% parameter(s) to be retrived
name = {'FWHM'; 'Position'};

% don't change below
DatasSize = DataSource.API_instancevaluesChunks('shot_id', shots, 'instance_id', instance, 'name', name);
Datas=[];
for ii=1:DatasSize
    Datas = [Datas;
        DataSource.API_instancevalues('shot_id', shots, 'instance_id', instance, 'name', name, 'chunk', ii)];
end
clear shots;