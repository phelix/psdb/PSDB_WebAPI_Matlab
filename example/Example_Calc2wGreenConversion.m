% Generate 2w Conversion Plot of Z6 long pulse and generate a plot
% only for experiment shots
% filtered by name 'Energy'
clearvars;
DataSource = PSDB;

% Define the experiment Name to ask for. You should have read permissions.
expName = {'P089'; 'U285'; 'U303'}; % 2w Experiments

% MAS Power 99, Z6 Green 155
% can be found with the method 'API_instancesList' of PSDB.
instances = [99 155];

d = gobjects(2,length(expName));
figure()
hold on
for ie = 1:length(expName)
    % get experiments list and find the experiment DB Id
    experiments = DataSource.API_experiments;
    expId = experiments(strcmpi(expName{ie}, {experiments.name})).id;
    if strcmpi(expName{ie}, 'U303')
        fsID = 13344; % bad tuned 2w crystal before
    else
        fsID = 1;
    end
    % get shot Id's that are 1 experiment shot
    tmpShotList = DataSource.API_shotsList('experiment_id', expId, 'shottype_id', 1, 'from_shot_id', fsID);

    % don't change below
    shots = [tmpShotList.id];
    clear tmpShotList experiments;
    DatasSize = DataSource.API_instancevaluesChunks('experiment_id', expId, 'shot_id', shots, 'instance_id', instances, 'name', 'Energy');
    Datas = [];
    for iii=1:DatasSize
        Datas = [Datas; DataSource.API_instancevalues('experiment_id', expId, 'shot_id', shots, 'instance_id', instances, 'name', 'Energy', 'chunk', iii)];
    end
    clear expId shots;
    ShotsWithEnergy = unique([Datas.shot_id]);
    Energy = struct('MAS',[],'Z62w',[]);
    for i = 1:length(ShotsWithEnergy)
        ret = Datas([Datas.shot_id] == ShotsWithEnergy(i));
        for ii = 1:length(ret)
            switch ret(ii).instance_id
                case instances(1)
                    Energy(i).MAS = str2double(ret(ii).data_numeric);
                case instances(2)
                    Energy(i).Z62w = str2double(ret(ii).data_numeric);
            end
        end
    end
    % filter for invalids
    Energy([Energy.Z62w] <= 0) = [];
    d(1,ie)= plot([Energy.MAS], [Energy.Z62w], '+', ...
        'DisplayName', expName{ie}, ...
        'MarkerSize', 8, 'LineWidth', 2);
    [p,~,mu] = polyfit([Energy.MAS], sqrt([Energy.Z62w]),1);
    X = min([Energy.MAS])-5:max([Energy.MAS])+15;
    d(2,ie)= plot(X, polyval(p,X,[],mu).^2, ...
        'DisplayName', [expName{ie} ' Fit']);
    d(2,ie).Color = d(1,ie).Color;
    d(2,ie).LineWidth = d(1,ie).LineWidth;
end
hold off
grid on
grid minor
legend show
legend('Location', 'best');
xlabel('MAS 1w IR Energy [J]');
ylabel('Z6 2w green Energy [J]');
